package ua.ithillel.dnipro.kremena.homework.service.impl;

import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.homework.jdbc.JdbcTemplate;
import ua.ithillel.dnipro.kremena.homework.mapper.UserRowMapper;
import ua.ithillel.dnipro.kremena.homework.model.User;
import ua.ithillel.dnipro.kremena.homework.service.UserService;
import ua.ithillel.dnipro.kremena.homework.utils.encoder.PasswordEncoder;
import ua.ithillel.dnipro.kremena.homework.utils.encoder.impl.MD5PasswordEncoder;

import java.util.List;

@RequiredArgsConstructor
public class DatabaseUserService implements UserService {

    private final JdbcTemplate jdbcTemplate;
    private final PasswordEncoder passwordEncoder;

    @Override
    public List<User> getAll() {
        return jdbcTemplate.query(
                "select id, login, date_born, date_registration from users order by login",
                new Object[0],
                new UserRowMapper()
        );
    }

    @Override
    public boolean add(User user) {
        return jdbcTemplate.update(
                "insert into users(login, password, date_born) values (?, ?, ?)",
                new Object[] {
                        user.getLogin(),
                        passwordEncoder.encode(user.getPassword()),
                        user.getDateBorn()
                }) == 1;
    }

    @Override
    public boolean delete(User user) {
        return jdbcTemplate.update(
                "delete from users where login = ?",
                new Object[] {
                        user.getLogin()
                }) == 1;
    }

    @Override
    public User getByLogin(String login) {
        return jdbcTemplate.query(
                "select id, login, password, date_born, date_registration from users where login = ?",
                new Object[] {
                        login
                },
                new UserRowMapper()
        ).stream()
                .findFirst()
                .orElse(new User());
    }
}