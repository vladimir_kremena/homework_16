package ua.ithillel.dnipro.kremena.homework.utils.properties;

import ua.ithillel.dnipro.kremena.homework.core.annotations.Bean;
import ua.ithillel.dnipro.kremena.homework.core.annotations.Configuration;

import java.util.Optional;
import java.util.Properties;

@Configuration
public class AppProperties {

    @Bean
    public Properties getProperties() {
        String applicationMode = System.getProperty("application.mode");
        applicationMode = Optional.ofNullable(applicationMode).orElse("IN_MEMORY");
        Properties properties = new Properties();
        properties.setProperty("application.mode", applicationMode);
        try {
            switch (applicationMode) {
                case "DB" :
                    properties.load(
                            getClass().getClassLoader().getResourceAsStream("config/database_config.properties"));
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Storage settings missing " + e.getMessage());
        }
        return properties;
    }
}