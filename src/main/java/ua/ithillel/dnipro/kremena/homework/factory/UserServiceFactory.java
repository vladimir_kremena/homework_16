package ua.ithillel.dnipro.kremena.homework.factory;

import ua.ithillel.dnipro.kremena.homework.jdbc.JdbcTemplate;
import ua.ithillel.dnipro.kremena.homework.service.UserService;
import ua.ithillel.dnipro.kremena.homework.utils.encoder.PasswordEncoder;

public interface UserServiceFactory {

    UserService createUserService(JdbcTemplate jdbcTemplate, PasswordEncoder passwordEncoder);

}