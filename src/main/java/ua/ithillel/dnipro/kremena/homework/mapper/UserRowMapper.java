package ua.ithillel.dnipro.kremena.homework.mapper;

import ua.ithillel.dnipro.kremena.homework.model.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.function.Function;

public class UserRowMapper implements Function<ResultSet, User> {

    @Override
    public User apply(ResultSet resultSet) {
        User user = new User();
        try {
            user.setId(resultSet.getLong("id"))
                    .setLogin(resultSet.getString("login"))
                    .setDateBorn(resultSet.getObject("date_born", LocalDate.class))
                    .setDateRegistration(resultSet.getObject("date_registration", LocalDateTime.class));
        } catch (SQLException sqlE) {
            sqlE.printStackTrace();
        }
        return user;
    }
}