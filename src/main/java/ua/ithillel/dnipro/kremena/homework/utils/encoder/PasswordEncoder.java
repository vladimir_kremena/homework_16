package ua.ithillel.dnipro.kremena.homework.utils.encoder;

public interface PasswordEncoder {

    String encode(String password);

    boolean verify(String password, String hash);

}