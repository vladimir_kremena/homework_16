package ua.ithillel.dnipro.kremena.homework.core.bean.definition;

import lombok.Value;

import java.lang.reflect.Method;

@Value
public class BeanDefinition {
    Object object;
    Method method;
    Class clazz;
    String id;
}