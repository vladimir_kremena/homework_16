package ua.ithillel.dnipro.kremena.homework.core.bean.factory;

import ua.ithillel.dnipro.kremena.homework.core.bean.definition.BeanDefinition;
import ua.ithillel.dnipro.kremena.homework.core.context.Context;
import ua.ithillel.dnipro.kremena.homework.core.exception.BeanNotFoundException;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;

public class BeanFactory {

    private List<BeanDefinition> definitions = new ArrayList<>();

    public void addDefinition(BeanDefinition definition) {
        definitions.add(definition);
    }

    private void createBean(Context context, BeanDefinition definition) {
        Method method = definition.getMethod();
        Parameter[] parameters = method.getParameters();
        Object[] arguments = new Object[parameters.length];
        for (int i = 0; i < parameters.length; i++) {
            try {
                arguments[i] = context.getBean(parameters[i].getType());
            } catch (Exception e) {
                BeanDefinition argumentDefinition = findBeanDefinition(parameters[i].getType());
                createBean(context, argumentDefinition);
                arguments[i] = context.getBean(parameters[i].getType());
            }
        }
        try {
            Object bean = method.invoke(definition.getObject(), arguments);
            context.registerBean(definition.getId(), bean);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private BeanDefinition findBeanDefinition(Class<?> clazz) {
        return  definitions.stream()
                .filter(definition -> definition.getClazz() == clazz)
                .findFirst()
                .orElseThrow(() -> new BeanNotFoundException("Bean " + clazz + " not found"));
    }

    public void createAllBeans(Context context) {
        definitions.forEach(definition -> createBean(context, definition));
    }
}