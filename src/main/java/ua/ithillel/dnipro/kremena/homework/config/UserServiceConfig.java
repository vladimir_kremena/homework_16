package ua.ithillel.dnipro.kremena.homework.config;

import ua.ithillel.dnipro.kremena.homework.core.annotations.Bean;
import ua.ithillel.dnipro.kremena.homework.core.annotations.Configuration;
import ua.ithillel.dnipro.kremena.homework.factory.UserServiceFactory;
import ua.ithillel.dnipro.kremena.homework.factory.impl.DatabaseUserServiceFactory;
import ua.ithillel.dnipro.kremena.homework.jdbc.JdbcTemplate;
import ua.ithillel.dnipro.kremena.homework.service.UserService;
import ua.ithillel.dnipro.kremena.homework.utils.encoder.PasswordEncoder;

import java.util.Map;
import java.util.Properties;

@Configuration
public class UserServiceConfig {

    @Bean
    public UserService userService(Properties properties, JdbcTemplate jdbcTemplate, PasswordEncoder passwordEncoder) {
        return  createUserServiceFactory(properties.getProperty("application.mode"))
                .createUserService(jdbcTemplate, passwordEncoder);
    }

    private UserServiceFactory createUserServiceFactory(String applicationMode) {
        Map<String, UserServiceFactory> factories = Map.of(
                "DB", new DatabaseUserServiceFactory()
        );
        return factories.get(applicationMode);
    }
}