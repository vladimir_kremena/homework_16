package ua.ithillel.dnipro.kremena.homework.factory.impl;

import ua.ithillel.dnipro.kremena.homework.factory.UserServiceFactory;
import ua.ithillel.dnipro.kremena.homework.jdbc.JdbcTemplate;
import ua.ithillel.dnipro.kremena.homework.service.UserService;
import ua.ithillel.dnipro.kremena.homework.service.impl.DatabaseUserService;
import ua.ithillel.dnipro.kremena.homework.utils.encoder.PasswordEncoder;

public class DatabaseUserServiceFactory implements UserServiceFactory {

    @Override
    public UserService createUserService(JdbcTemplate jdbcTemplate, PasswordEncoder passwordEncoder) {
        return new DatabaseUserService(jdbcTemplate, passwordEncoder);
    }
}