package ua.ithillel.dnipro.kremena.homework.config;

import ua.ithillel.dnipro.kremena.homework.core.annotations.Bean;
import ua.ithillel.dnipro.kremena.homework.core.annotations.Configuration;
import ua.ithillel.dnipro.kremena.homework.utils.encoder.PasswordEncoder;
import ua.ithillel.dnipro.kremena.homework.utils.encoder.impl.MD5PasswordEncoder;

@Configuration
public class PasswordEncoderConfig {

    @Bean
    public PasswordEncoder createPasswordEncoder() {
        return new MD5PasswordEncoder();
    }
}