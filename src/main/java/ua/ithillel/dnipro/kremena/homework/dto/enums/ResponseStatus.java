package ua.ithillel.dnipro.kremena.homework.dto.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ResponseStatus {
    OK, ERROR;

    @JsonCreator
    public static ResponseStatus forValue(String value) {
        for (ResponseStatus responseStatus : ResponseStatus.values()) {
            if (responseStatus.toValue().equalsIgnoreCase(value)) {
                return responseStatus;
            }
        }
        return null;
    }

    @JsonValue
    public String toValue() {
        return name();
    }
}