package ua.ithillel.dnipro.kremena.homework.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ua.ithillel.dnipro.kremena.homework.core.annotations.Bean;
import ua.ithillel.dnipro.kremena.homework.core.annotations.Configuration;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
public class DatasourceConfig {

    @Bean
    public DataSource createDatasource(Properties properties) {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(properties.getProperty("driver.class.name"));
        hikariConfig.setJdbcUrl(properties.getProperty("database.uri"));
        hikariConfig.setUsername(properties.getProperty("database.user"));
        hikariConfig.setPassword(properties.getProperty("database.password"));
        hikariConfig.setMinimumIdle(5);
        hikariConfig.setMaximumPoolSize(10);
        return new HikariDataSource(hikariConfig);
    }
}