package ua.ithillel.dnipro.kremena.homework.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import ua.ithillel.dnipro.kremena.homework.core.context.Context;
import ua.ithillel.dnipro.kremena.homework.core.initializer.ApplicationInitializer;
import ua.ithillel.dnipro.kremena.homework.core.scanner.PackageScanner;
import ua.ithillel.dnipro.kremena.homework.dto.UserAddResponse;
import ua.ithillel.dnipro.kremena.homework.dto.UserDelResponse;
import ua.ithillel.dnipro.kremena.homework.dto.UsersResponse;
import ua.ithillel.dnipro.kremena.homework.dto.enums.ResponseStatus;
import ua.ithillel.dnipro.kremena.homework.model.User;
import ua.ithillel.dnipro.kremena.homework.service.UserService;
import ua.ithillel.dnipro.kremena.homework.utils.validator.Validator;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

@WebServlet(urlPatterns = {"/json/users", "/json/useradd", "/json/userdel",
        "/web/users", "/web/useradd", "/web/userdel"})
public class IndexServlet extends HttpServlet {

    private final UserService userService;
    private final ObjectMapper objectMapper;
    private final Validator validator;

    public IndexServlet() {
        PackageScanner packageScanner = new PackageScanner();
        ApplicationInitializer applicationInitializer = new ApplicationInitializer(packageScanner);
        Context context = applicationInitializer.createContext("ua.ithillel.dnipro.kremena.homework");
        userService = context.getBean(UserService.class);
        objectMapper = context.getBean(ObjectMapper.class);
        validator = context.getBean(Validator.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        switch (req.getServletPath()) {
            case "/json/users" :
                jsonGet(resp);
                break;
            case "/web/users" :
                webGet(req, resp);
                break;
            default :
                super.doGet(req, resp);
                break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        switch (req.getServletPath()) {
            case "/json/useradd" :
                jsonAdd(req, resp);
                break;
            case "/web/useradd" :
                webAdd(req, resp);
                break;
            case "/web/userdel" :
                webDel(req, resp);
                break;
            default :
                super.doPost(req, resp);
                break;
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        switch (req.getServletPath()) {
            case "/json/userdel" :
                jsonDel(req, resp);
                break;
            default :
                super.doDelete(req, resp);
                break;
        }
    }

    private void jsonGet(HttpServletResponse resp) throws IOException {
        resp.setHeader("Content-type", "application/json");
        List<User> users = userService.getAll();
        if (users.size() != 0) {
            resp.getOutputStream().write(
                    objectMapper.writeValueAsBytes(
                            UsersResponse.builder()
                                    .responseStatus(ResponseStatus.OK)
                                    .users(users)
                                    .build()
                    )
            );
        } else {
            resp.getOutputStream().write(
                    objectMapper.writeValueAsBytes(
                            UsersResponse.builder()
                                    .responseStatus(ResponseStatus.ERROR)
                                    .error("User list is empty")
                                    .build()
                    )
            );
        }
        resp.getOutputStream().flush();
    }

    private void jsonAdd(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setHeader("Content-type", "application/json");
        User incomingUser = objectMapper.readValue(req.getInputStream().readAllBytes(), User.class);
        if (validator.isValid(incomingUser)) {
            User databaseUser = userService.getByLogin(incomingUser.getLogin());
            if (incomingUser.getLogin().equals(databaseUser.getLogin())) {
                resp.getOutputStream().write(objectMapper.writeValueAsBytes(
                        UserAddResponse.builder()
                                .responseStatus(ResponseStatus.ERROR)
                                .error("Login is already taken")
                                .build()
                        )
                );
            } else {
                if (userService.add(incomingUser)) {
                    resp.getOutputStream().write(objectMapper.writeValueAsBytes(
                            UserAddResponse.builder()
                                    .responseStatus(ResponseStatus.OK)
                                    .build()
                            )
                    );
                }
            }
        } else {
            resp.getOutputStream().write(objectMapper.writeValueAsBytes(
                    UserAddResponse.builder()
                            .responseStatus(ResponseStatus.ERROR)
                            .error(validator.fullnessCheck(incomingUser))
                            .build()
                    )
            );
        }
        resp.getOutputStream().flush();
    }

    private void jsonDel(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setHeader("Content-type", "application/json");
        User incomingUser = objectMapper.readValue(req.getInputStream().readAllBytes(), User.class);
        if (userService.delete(incomingUser)) {
            resp.getOutputStream().write(objectMapper.writeValueAsBytes(
                    UserDelResponse.builder()
                            .responseStatus(ResponseStatus.OK)
                            .build()
                    )
            );
        } else {
            resp.getOutputStream().write(objectMapper.writeValueAsBytes(
                    UserDelResponse.builder()
                            .responseStatus(ResponseStatus.ERROR)
                            .error("User is not found")
                            .build()
                    )
            );
        }
        resp.getOutputStream().flush();
    }

    private void webGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("message") != null) {
            req.setAttribute("message", req.getParameter("message"));
        }
        req.setAttribute("users", userService.getAll());
        getServletContext().getRequestDispatcher("/WEB-INF/views/users.jsp").forward(req, resp);
    }

    private void webAdd(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User addUser = new User();
        try {
            addUser.setLogin(req.getParameter("login"));
            addUser.setPassword(req.getParameter("password"));
            addUser.setDateBorn(LocalDate.parse(req.getParameter("dateBorn"),
                    DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        } catch (DateTimeParseException dTPE) {
            addUser.setLogin(req.getParameter("login"));
            addUser.setPassword(req.getParameter("password"));
            addUser.setDateBorn(LocalDate.of(1970, 1, 1));
        }
        if (validator.isValid(addUser)) {
            User databaseUser = userService.getByLogin(addUser.getLogin());
            if (addUser.getLogin().equals(databaseUser.getLogin())) {
                req.setAttribute("message", "Login is already taken");
            } else {
                if (userService.add(addUser)) {
                    req.setAttribute("message", "User " + addUser.getLogin() + " added");
                } else {
                    req.setAttribute("message", "User " + addUser.getLogin() + " not added");
                }
            }
        } else {
            req.setAttribute("message", validator.fullnessCheck(addUser));
        }
        getServletContext().getRequestDispatcher("/WEB-INF/views/buffer.jsp").forward(req, resp);
    }

    private void webDel(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User delUser = new User();
        delUser.setLogin(req.getParameter("login"));
        if (userService.delete(delUser)) {
            req.setAttribute("message", "User " + delUser.getLogin() + " deleted");
        } else {
            req.setAttribute("message", "User " + delUser.getLogin() + " is not found");
        }
        getServletContext().getRequestDispatcher("/WEB-INF/views/buffer.jsp").forward(req, resp);
    }
}