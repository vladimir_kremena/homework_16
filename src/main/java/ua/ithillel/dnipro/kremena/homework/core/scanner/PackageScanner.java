package ua.ithillel.dnipro.kremena.homework.core.scanner;

import java.io.File;
import java.lang.annotation.Annotation;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PackageScanner {

    public List<Class<?>> findAll(String packageName) {
        String path = packageName.replace(".", "/");
        URL res = getClass().getClassLoader().getResource(path);
        List<Class<?>> classes = new ArrayList<>();
        try {
            File packageDir = new File(res.toURI());
            for (File file : packageDir.listFiles()) {
                String fileName = file.getName();
                if (file.isFile() && fileName.endsWith(".class")) {
                    String className = getClassName(packageName, fileName);
                    classes.add(Class.forName(className));
                } else if (file.isDirectory()) {
                    List<Class<?>> innerClasses = findAll(packageName + "." + fileName);
                    classes.addAll(innerClasses);
                }
            }
        } catch (URISyntaxException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return classes;
    }

    public List<Class<?>> findWithAnnotation(String packageName, Class<? extends Annotation> annotationClass) {
        return  findAll(packageName).stream()
                .filter(clazz -> clazz.isAnnotationPresent(annotationClass))
                .collect(Collectors.toList());
    }

    private String getClassName(String packageName, String fileName) {
        return packageName + "." + fileName.substring(0, fileName.length() - 6);
    }

}