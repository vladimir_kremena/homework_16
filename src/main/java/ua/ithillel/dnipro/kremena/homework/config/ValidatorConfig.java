package ua.ithillel.dnipro.kremena.homework.config;

import ua.ithillel.dnipro.kremena.homework.core.annotations.Bean;
import ua.ithillel.dnipro.kremena.homework.core.annotations.Configuration;
import ua.ithillel.dnipro.kremena.homework.model.User;
import ua.ithillel.dnipro.kremena.homework.utils.validator.Validator;
import ua.ithillel.dnipro.kremena.homework.utils.validator.impl.UserValidator;

@Configuration
public class ValidatorConfig {

    @Bean
    public Validator<User> createValidator() {
        return new UserValidator();
    }
}