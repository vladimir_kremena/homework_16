package ua.ithillel.dnipro.kremena.homework.core.initializer;

import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.homework.core.annotations.Bean;
import ua.ithillel.dnipro.kremena.homework.core.annotations.Configuration;
import ua.ithillel.dnipro.kremena.homework.core.bean.definition.BeanDefinition;
import ua.ithillel.dnipro.kremena.homework.core.bean.factory.BeanFactory;
import ua.ithillel.dnipro.kremena.homework.core.context.Context;
import ua.ithillel.dnipro.kremena.homework.core.scanner.PackageScanner;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RequiredArgsConstructor
public class ApplicationInitializer {

    private final PackageScanner packageScanner;
    private String packageName;

    public Context createContext(String packageName) {
        this.packageName = packageName;
        Context context = new Context();
        List<Object> configurations = createConfiguration();
        configurations.forEach(
                configuration -> context.registerBean(configuration.getClass().getSimpleName(), configuration));
        createConfigurationBeans(context, configurations);
        return context;
    }

    private List<Object> createConfiguration() {
        List<Class<?>> configClasses = packageScanner.findWithAnnotation(packageName, Configuration.class);
        List<Object> configObjects = new ArrayList<>(configClasses.size());
        for (Class<?> configClass : configClasses) {
            try {
                Constructor<?> constructor = configClass.getConstructor();
                Object object = constructor.newInstance();
                configObjects.add(object);
            } catch (Exception e) {
                throw new RuntimeException("Error loading configuration: " + configClass.getName(), e);
            }
        }
        return configObjects;
    }

    private void createConfigurationBeans(Context context, List<Object> configurations) {
        BeanFactory beanFactory = new BeanFactory();
        for (Object configuration : configurations) {
            Arrays.stream(configuration.getClass().getMethods())
                    .filter(method -> method.isAnnotationPresent(Bean.class))
                    .map(method -> createBeanDefinition(configuration, method))
                    .forEach(beanFactory::addDefinition);
        }
        beanFactory.createAllBeans(context);
    }

    private BeanDefinition createBeanDefinition(Object configuration, Method method) {
        Bean beanAnnotation = method.getAnnotation(Bean.class);
        String beanName = beanAnnotation.id().isEmpty() ? method.getName() : beanAnnotation.id();
        return new BeanDefinition(configuration, method, method.getReturnType(), beanName);
    }
}