package ua.ithillel.dnipro.kremena.homework.utils.validator;

public interface Validator<T> {

    boolean isValid(T t);

    String fullnessCheck(T t);

}