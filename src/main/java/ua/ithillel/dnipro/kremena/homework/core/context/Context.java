package ua.ithillel.dnipro.kremena.homework.core.context;

import ua.ithillel.dnipro.kremena.homework.core.exception.BeanNotFoundException;

import java.util.HashMap;
import java.util.Map;

public class Context {

    private Map<String, Object> beans = new HashMap<>();

    public void registerBean(String beanId, Object bean) {
        beans.put(beanId, bean);
    }

    public Object getBean(String beanId) {
        return beans.get(beanId);
    }

    public<T> T getBean(Class<T> clazz) {
        return  beans.values().stream()
                .filter(object -> clazz.isAssignableFrom(object.getClass()))
                .map(object -> (T)object)
                .findFirst()
                .orElseThrow(() -> new BeanNotFoundException("Bean with class " + clazz + " not found"));
    }

    // TODO: create method delete beans !!!

    @Override
    public String toString() {
        return  "Context [" +
                "beans = " + beans +
                "]";
    }
}